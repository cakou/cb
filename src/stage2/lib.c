/*
 * Copyright (C) 2013  Camille Lecuyer <camille.lecuyer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street,
 * Fifth Floor,Boston, MA  02110-1301, USA.
 */


/*
 * This file is based on kernel.c from multiboot specification.
 * Some modifications have been done to fit our needs
 * like data sections.
 * Some functions as been added.
 */

#include "tools.h"
#include "types.h"

/* Some screen stuff. */
/* The number of columns. */
#define COLUMNS                 80
/* The number of lines. */
#define LINES                   24
/* The attribute of an character. */
#define ATTRIBUTE               7
/* The video memory address. */
#define VIDEO                   0xB8000

/* Variables. */
/* Save the X position. */
extern int xpos;
/* Save the Y position. */
extern  int ypos;
/* Point to the video memory. */
extern volatile unsigned char *video;



/* Clear the screen and initialize VIDEO, XPOS and YPOS. */
void
cls (void)
{
  int i;

  video = (unsigned char *) VIDEO;

  for (i = 0; i < COLUMNS * LINES * 2; i++)
    *(video + i) = 0;

  xpos = 0;
  ypos = 0;
}

/* Convert the integer D to a string and save the string in BUF. If
   BASE is equal to 'd', interpret that D is decimal, and if BASE is
   equal to 'x', interpret that D is hexadecimal. */
static void
itoa (char *buf, int base, int d)
{
  char *p = buf;
  char *p1, *p2;
  unsigned long ud = d;
  int divisor = 10;

  /* If %d is specified and D is minus, put `-' in the head. */
  if (base == 'd' && d < 0)
    {
      *p++ = '-';
      buf++;
      ud = -d;
    }
  else if (base == 'x')
    divisor = 16;

  /* Divide UD by DIVISOR until UD == 0. */
  do
    {
      int remainder = ud % divisor;

      *p++ = (remainder < 10) ? remainder + '0' : remainder + 'a' - 10;
    }
  while (ud /= divisor);

  /* Terminate BUF. */
  *p = 0;

  /* Reverse BUF. */
  p1 = buf;
  p2 = p - 1;
  while (p1 < p2)
    {
      char tmp = *p1;
      *p1 = *p2;
      *p2 = tmp;
      p1++;
      p2--;
    }
}

/* Put the character C on the screen. */
static void
putchar (int c)
{
  if (c == '\n' || c == '\r')
    {
    newline:
      xpos = 0;
      ypos++;
      if (ypos >= LINES)
        ypos = 0;
      return;
    }

  *(video + (xpos + ypos * COLUMNS) * 2) = c & 0xFF;
  *(video + (xpos + ypos * COLUMNS) * 2 + 1) = ATTRIBUTE;

  xpos++;
  if (xpos >= COLUMNS)
    goto newline;
}

/* Format a string and print it on the screen, just like the libc
   function printf. */
void
printf (const char *format, ...)
{
  char **arg = (char **) &format;
  int c;
  char buf[20];

  arg++;

  while ((c = *format++) != 0)
    {
      if (c != '%')
        putchar (c);
      else
        {
          char *p;

          c = *format++;
          switch (c)
            {
            case 'd':
            case 'u':
            case 'x':
              itoa (buf, c, *((int *) arg++));
              p = buf;
              goto string;
              break;

            case 's':
              p = *arg++;
              if (! p)
                p = "(null)";

            string:
              while (*p)
                putchar (*p++);
              break;

            default:
              putchar (*((int *) arg++));
              break;
            }
        }
    }
}


int	strncmp(char *s1, char *s2, unsigned int n)
{
  int	i;

  for (i = 0; i < n && s1[i] && s2[i] && s1[i] == s2[i]; i++)
    i++;
  if (i >= n)
    return 0;
  if (s1[i] < s2[i])
    return -1;
  // last case
  return 1;
}

void*	memcpy(void* dst, const void* src, unsigned int n)
{
  const char* s = src;
  char*	d = dst;

  for (; n > 0; n--, d++, s++)
    *d = *s;

  return (dst);
}



void hang(const char	*s, ...)
{
  printf(s);

  asm ("hlt");
  while (1)
    ;
}


void clean_screen(void)
{
  int	i;
  char	*s = (char*)0xB8000;

  for (i = 0; i < 25 * 80 * 2; i += 2)
    s[i] = 0;
}

/*
 * size: size of file in byte.
 */
void load_file(uint32_t	addr,
	       uint64_t	lba,
	       uint32_t	size)
{
  int	sectors;
  int	i;
  dap_t dap =
  {
    .size = 16,
    .sectors = 32,	// buffer size = 0x4000
    .dest_offset = 0x8000,
    .lba_address = lba,
  };

  sectors = size / 512;
  if (size % 512)
    sectors++;		// take one more sectors for few bytes.

  // FIXME: use define. 0x4000 = buffer size
  if (sectors >= 32)	// load packet of 32 sectors
  {
    for (i = 0; i < sectors - 32; i += 32)
      {
	read_on_disk((void*)&dap, addr);
	addr += 0x4000;
	dap.lba_address += 32;	// 0x4000 = 32 sectors

	printf("*");
      }
  }
  if (sectors % 32)	// load remaining sectors
  {
    dap.sectors = sectors % 32;
    read_on_disk((void*)&dap, addr);
  }
  printf("*\n");
}

