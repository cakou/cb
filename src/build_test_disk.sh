#!/bin/sh


STAGE1=stage1/stage1
STAGE2=stage2/stage2

source $(pwd)/"Makefile.config"


echo -e "\n    Generate image $DISK..."

cp ${STAGE1} ${DISK}
dd if=${STAGE2} of=${DISK} bs=512 seek=1 count=17 2> /dev/null

dd if=${KERNEL} of=${DISK} bs=512 seek=$KERNEL_LBA 2> /dev/null
# align the image on 512B. FIXME: do it well.
dd if=${INITRD}  of=${DISK} bs=512 seek=$INITRD_LBA 2> /dev/null
dd if=/dev/zero bs=1024 count=2 >> ${DISK} 2> /dev/null

echo -e "    Done."
