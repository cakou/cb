#ifndef LINUX_H_
# define LINUX_H_

# include "types.h"

/*
 * This file has been created following the linux/x86 boot protocol.
 * See Documentation/boot.txt for more information.
 * A copy of this file is provided under repo/doc.
 */

// define the header magic number
# define HDRS	0x53726448

// define the loadflags
# define LOAD_LOADED_HIGH	(1 << 0)
# define LOAD_QUIET_FLAG	(1 << 5)
# define LOAD_KEEP_SEGMENTS	(1 << 6)
# define LOAD_CAN_USE_HEAP	(1 << 7)


typedef struct
{
  uint8_t	setup_sects;	// The size of the setup in sectors
  uint16_t	root_flags;	// If set, the root is mounted readonly
  uint32_t	syssize;	// The size of the 32-bit code in 16-byte paras
  uint16_t	ram_size;	// DO NOT USE - for bootsect.S use only
  uint16_t	vid_mode;	// Video mode control
  uint16_t	root_dev;	// Default root device number
  uint16_t	boot_flag;	// 0xAA55 magic number
  uint16_t	jump;		// Jump instruction
  uint32_t	header;		// Magic signature "HdrS"
  uint16_t	version;	// Boot protocol version supported
  uint32_t	realmode_swtch;	// Boot loader hook (see below)
  uint16_t	start_sys_seg;	// The load-low segment (0x1000) (obsolete)
  uint16_t	kernel_version;	// Pointer to kernel version string
  uint8_t	type_of_loader;	// Boot loader identifier
  uint8_t	loadflags;	// Boot protocol option flags
  uint16_t	setup_move_size;// Move to high memory size (used with hooks)
  uint32_t	code32_start;	// Boot loader hook (see below)
  uint32_t	ramdisk_image;	// initrd load address (set by boot loader)
  uint32_t	ramdisk_size;	// initrd size (set by boot loader)
  uint32_t	bootsect_kludge;// DO NOT USE - for bootsect.S use only
  uint16_t	heap_end_ptr;	// Free memory after setup end
  uint8_t	ext_loader_ver;	// Extended boot loader version
  uint8_t	ext_loader_type;// Extended boot loader ID
  uint32_t	cmd_line_ptr;	// 32-bit pointer to the kernel command line
  uint32_t	ramdisk_max;	// Highest legal initrd address
  uint32_t	kernel_alignment; // Physical addr alignment required for kernel
  uint8_t	relocatable_kernel;	// Whether kernel is relocatable or not
  uint8_t	min_alignment;	// Minimum alignment, as a power of two
  uint16_t	xloadflags;	// Boot protocol option flags
  uint32_t	cmdline_size;	// Maximum size of the kernel command line
  uint32_t	hardware_subarch;	//  Hardware subarchitecture
  uint64_t	hardware_subarch_data;	// Subarchitecture-specific data
  uint32_t	payload_offset;	// Offset of kernel payload
  uint32_t	payload_length;	// Length of kernel payload
  uint64_t	setup_data;	// 64-bit physical pointer to linked list
				// of struct setup_data
  uint64_t	pref_address;	// Preferred loading address
  uint32_t	init_size;	// Linear memory required during initialization
  uint32_t	handover_offset;// Offset of handover entry point

} __attribute__ ((packed)) linux_header;



void linux_load(unsigned int		addr);

void linux_boot(void);

#endif /* !LINUX_H_ */
