#ifndef LIB_H_
# define LIB_H_

# include "types.h"

# define MEM_TYPE_NORMAL		1
# define MEM_TYPE_RESERVED		2
# define MEM_TYPE_ACPI_RECLAIMABLE	3
# define MEM_TYPE_ACPI_NVS		4
# define MEM_TYPE_BAD_MEMORY		5

typedef struct
{
  uint64_t	base_addr;
  uint64_t	size;
  uint32_t	type;
  uint32_t	acpi;	// optionnal
} __attribute__ ((packed)) upper_mem_t;

int	strncmp(char		*s1,
		char		*s2,
		unsigned int	n);

void	printf(const char	*format, ...);

void*	memcpy(void*		dst,
	       const void*	src,
	       unsigned int	n);

void	cls(void);

void	load_file(uint32_t	addr,
		  uint64_t	lba,
		  uint32_t	size);

void	hang(const char		*s, ...);

void	clean_screen(void);

#endif /* !LIB_H_ */
