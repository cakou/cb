#ifndef TOOLS_H_
# define TOOLS_H_

# include "types.h"

void read_on_disk(void* dap, int addr);

char read_key(void);

// Disk Address Packet
// Used to read on disk.
typedef struct
{
  uint8_t	size;
  uint8_t	zero;
  uint16_t	sectors;
  uint16_t	dest_offset;
  uint16_t	dest_seg;
  uint64_t	lba_address;
} __attribute__ ((packed)) dap_t;


#endif /* !TOOLS_H_ */
