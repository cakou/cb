/*
 * Copyright (C) 2013  Camille Lecuyer <camille.lecuyer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street,
 * Fifth Floor,Boston, MA  02110-1301, USA.
 */

#include "multiboot.h"
#include "linux.h"
#include "lib.h"
#include "elf.h"
#include "tools.h"

// define and filled in stage2.S in 16bit code.
extern uint32_t memory_low;
extern uint32_t memory_upper_list_sz;

/* Check if the bit BIT in FLAGS is set. */
#define CHECK_FLAG(flags,bit)   ((flags) & (1 << (bit)))

void load_kernel_elf_section(Elf32_Ehdr		*header,
			     Elf32_Shdr		*section_header)
{
  printf("Load section: type = %u, flags = %u\n",
	 section_header->sh_type,
	 section_header->sh_flags);
  memcpy((void*)section_header->sh_addr, (void*)((int)header + section_header->sh_offset), section_header->sh_size);
}


/*
 * Load the given program elf header from disk.
 * @header: the elf header.
 * @program_header: the program header to load.
 */
void load_kernel_elf_prg(Elf32_Ehdr		*header,
			 Elf32_Phdr		*program_header)
{
  int	sect_prog;	// the sector position in the OS image.

  sect_prog = program_header->p_offset / 512;

#ifdef DEBUG
  printf("Load program: type = %u, flags = %u, size = %u, sector = %u\n",
	 program_header->p_type,
	 program_header->p_flags,
	 program_header->p_filesz,
	 sect_prog);
#endif /* !DEBUG */

  if (program_header->p_offset % 512)
    printf("Program not align to 512Bytes!\n");

  // FIXME: handle unaligned program header on file!
  // quick and dirty fix:
  load_file(program_header->p_paddr - program_header->p_offset % 512,
	    KERNEL_LBA + sect_prog,
	    program_header->p_filesz + program_header->p_offset % 512);
  //memcpy(program_header->p_paddr, (int)header + program_header->p_offset, program_header->p_filesz);
}


/*
 * Load the kernel in higher memory.
 * Return the entry point.
 * Return also the top address used by the kernel in top_addr.
 * (used to load the module).
 */
void *load_kernel_elf(unsigned int	addr,
		      uint32_t		*top_addr)
{
  int				i;
  Elf32_Ehdr			*header;
  Elf32_Phdr			*program_header;
  
  header = (Elf32_Ehdr*)addr;
  *top_addr = 0;

  printf("Loading kernel elf file...\n");

  if (strncmp((char*)header->e_ident, ELFMAG, SELFMAG))
    hang("The given kernel isn't an elf file\n"
	 "Magic = 0x%x", (int)header->e_ident);

  program_header = (Elf32_Phdr*)(addr + header->e_phoff);
  for (i = 0; i < header->e_phnum; i++)
  {
    if (program_header[i].p_type == PT_LOAD)
    {
      load_kernel_elf_prg(header, &program_header[i]);
      if (program_header[i].p_vaddr + program_header[i].p_memsz > *top_addr)
	*top_addr = program_header[i].p_vaddr + program_header[i].p_memsz;
    }
  }

  //return (void*)program_header[0].p_paddr;
#warning "The entry point for multiboot is no well defined"
  return (void*)header->e_entry;
}


void jump_entry_point(struct multiboot_info	*info,
		      void*			entry)
{
  asm ("mov %0, %%eax\n"
       "mov %1, %%ebx\n"
       "jmp *%2\n"
       :
       : "r" (MULTIBOOT_BOOTLOADER_MAGIC),
	 "r" (info),
	 "r" (entry)
       :);
}

void multiboot_load_modules(struct multiboot_info	*info,
			    multiboot_module_t		*mod_list,
			    uint32_t			top_kernel_addr,
			    int				align_modules)
{
  uint32_t	module_load_addr;


  // check if we have to align the modules to a page.
  if (align_modules && top_kernel_addr % 0x1000)
    module_load_addr = top_kernel_addr + 0x1000 - (top_kernel_addr % 0x1000);
  else
    module_load_addr = top_kernel_addr;


  // load the module from hdd into memory.
  load_file(module_load_addr, INITRD_LBA, INITRD_SIZE);

  // set multiboot info: we say that we have one module.
  info->flags |= MULTIBOOT_INFO_MODS;
  info->mods_count = 1;
  info->mods_addr = (multiboot_uint32_t)mod_list;

  // set module attributes.
  mod_list->mod_start = module_load_addr;
  mod_list->mod_end = module_load_addr + INITRD_SIZE;
  mod_list->cmdline = (multiboot_uint32_t)"foo";	// FIXME: use define
  mod_list->pad = 0;
}


/*
 * Fill memory information into @info.
 */

void multiboot_memory_mapping(struct multiboot_info	*info)
{
  upper_mem_t			*map = (upper_mem_t*)0x7010; // FIXME: use define
  multiboot_memory_map_t	*multiboot_map;
  uint64_t			total = 0;
  int				i;

  // tell to the OS that mapping is avaliable
  info->flags |= MULTIBOOT_INFO_MEM_MAP;
  info->mmap_addr = 0x7010 - 4;	// FIXME: use define
  info->mmap_length = memory_upper_list_sz * sizeof (multiboot_memory_map_t);

  multiboot_map = (multiboot_memory_map_t*)info->mmap_addr;

#ifdef DEBUG
  printf("Size of memory list: %u\n", memory_upper_list_sz);
#endif /* !DEBUG */

  // browse mapping and fill it as multiboot spec need.
  for (i = 0; i < memory_upper_list_sz; i++)
  {
#ifdef DEBUG
    printf("  base addr: %u, size: %u, type: %u, acpi: %u\n",
	   (int)map->base_addr, (int)map->size, map->type,
	   (int)map->acpi);
#endif /* !DEBUG */

    if (map->type == MEM_TYPE_NORMAL)
      total += map->size;

    multiboot_map->size = sizeof (upper_mem_t) - sizeof (multiboot_map->size);
    multiboot_map++;
    map++;
  }
  info->mem_upper = (uint32_t)total / 1024;

  // Fill lower memory info
  info->mem_lower = memory_low;

  info->flags |= MULTIBOOT_INFO_MEMORY;

  printf("Lower memory: %ukB\n", info->mem_lower);
  printf("Upper memory: %ukB\n", info->mem_upper);
}

/*
 * Check the multiboot magic and fill the multiboot info.
 * If the multiboot magic is found, load this kernel.
 * If not, try to load a linux kernel.
 *
 * addr = address of the kernel's image into memory.
 * addr must be 32bit align.
 */
void multiboot(unsigned int		addr,
	       struct multiboot_info	*info)
{
  struct multiboot_header	*header;
  int				*img = (int*)addr;
  int				i;
  int				align_modules = 0;
  void				*entry_point;
  multiboot_module_t		mod_list;	// handle only one module.
  uint32_t			top_kernel_addr;

  // seek the header magic into the image.
  for (i = 0; i < MULTIBOOT_SEARCH / 4; i++)
    if (img[i] == MULTIBOOT_HEADER_MAGIC)
      break;

  // if no multiboot magic number found, try to load a linux kernel.
  if (img[i] != MULTIBOOT_HEADER_MAGIC)
    linux_load(addr);
  printf("Loading a multiboot kernel\n");
  header = (struct multiboot_header*)&img[i];

  printf("multiboot magic pos = %u\n", i * 4);
  if ((header->magic + header->flags + header->checksum) != 0)
    hang("Kernel's image error: bad multiboot checksum");

  if (CHECK_FLAG(header->flags, MULTIBOOT_PAGE_ALIGN))
    align_modules = 1;

  // FIXME: set the graphic mode from header.

  // IF already cleared in stage2.S

  info->flags = MULTIBOOT_INFO_CMDLINE;

  info->cmdline = (multiboot_uint32_t)"hello";	// FIXME: take it from somewhere

  multiboot_memory_mapping(info);

  entry_point = load_kernel_elf(addr, &top_kernel_addr);

  printf("Loading modules\n");
  multiboot_load_modules(info, &mod_list, top_kernel_addr, align_modules);

  printf("Entry point = 0x%x\n", entry_point);
  printf("Hit a key to run the kernel\n");
  read_key();
  clean_screen();
  jump_entry_point(info, entry_point);
  hang("Load Multiboot failed\n");
}


