CB - Cakou's Bootloader
=======================

A simple Linux and Multiboot bootloader with 2 stages.


You can boot a Linux kernel or a Multiboot kernel.
CB only manage one module.
You can build a simple disk image which can be used in qemu
or you can use it with a usb stick (just use dd utilitary).
You can also use it on your own hard drive adding 'inplace'
at the end of the configure line, but it's not fully supported yet.
So I don't recommend this.


HOWTO BUILD:
-----------
      cd src
      ./configure YourKernelFile YourModule [ImageDiskName] [inplace]
      make

You can use /boot/vmlinuz* as your kernel file and /boot/initramfs.img as
your module, but it depends of your distro.


HOWTO TEST:
----------
      make boot
or
      qemu -hda disk.img

