/*
 * Copyright (C) 2013  Camille Lecuyer <camille.lecuyer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street,
 * Fifth Floor,Boston, MA  02110-1301, USA.
 */

#include "lib.h"
#include "linux.h"
#include "tools.h"

#ifndef INITRD_LBA     
// FIXME
# define INITRD_LBA	8192	// skip 4MB in sectors
# define INITRD_SIZE	3145728	// 3Mo
#endif


/*
 * Load the protected kernel from hdd in high memory.
 * We use bios call with LBA.
 */
static void linux_load_protected(linux_header	*header)
{
  int		is_bzImage;
  uint32_t	load_address;
  uint32_t	protected_code_size;
  uint64_t	lba_address;

  lba_address = KERNEL_LBA;	// base of the linux image.

  is_bzImage = (header->version >= 0x0200)
    && (header->loadflags & LOAD_LOADED_HIGH);

  load_address = is_bzImage ? 0x100000 : 0x10000;

  lba_address += header->setup_sects + 1;	// start of protected code.

  protected_code_size = header->syssize * 16;
  load_file(load_address, lba_address, protected_code_size);
}


static void linux_fill_default(linux_header	*header)
{
  char		*cmd_line = "debug ";// console=tty0 console=ttyS0,9600n8 edd=off";
  uint32_t	initrd_pos;
  int		i = 0;
  
  
  // fill vid_mode
  header->vid_mode = 0xFFFF;	// normal video mode

  // fill bootloader type
  header->type_of_loader = 0xFF;	// no id assign (yet...)

  // allow the linux kernel to use the provided stack with heap_end_ptr
  header->loadflags |= LOAD_CAN_USE_HEAP;

  // fill setup_move_size
  if (header->version < 0x202)
    header->setup_move_size = 0;	// FIXME: set a true value.

  // Initramfs stuff.
  initrd_pos = 0x100000 + header->syssize * 16;
  // we position the initrd on the next MB (0x100000) avaliable.
  if (initrd_pos % 0x100000 != 0)
    initrd_pos += 0x100000 - (initrd_pos % 0x100000);
  header->ramdisk_image = initrd_pos;
  header->ramdisk_size = INITRD_SIZE;

  header->heap_end_ptr = 0x10000 - 0x200;    // real mode stack (cf boot.txt)

  header->cmd_line_ptr = (int)header - 0x1F1 + 0x10000;     // cf boot.txt example.

  while (cmd_line[i])
  {
    ((char*)header->cmd_line_ptr)[i] = cmd_line[i];
    i++;
  }
  ((char*)header->cmd_line_ptr)[i] = 0;

  // FIXME: get the string from somewhere and copy it
  // at the good location.
  
  header->hardware_subarch = 0;	// default x86/PC environment.
}

void linux_load(unsigned int		addr)
{
  linux_header	*header;
  int		i = 0;

  printf("Try to load a linux kernel...\n");

  header = (linux_header*)(addr + 0x01f1); // start of setup header

  printf("Linux boot protocol: %u.%u\n", (header->version >> 8),
	 (header->version & 0xFF));
  if (header->header != HDRS)
    hang("The linux kernel is too old (< 2.0)");

  if (header->kernel_version + 512 <= header->setup_sects * 512)
    printf("Linux kernel version: %s\n",
	   (char*)(addr + header->kernel_version + 512));

  linux_fill_default(header);

  // Load protected code in upper memory.
  linux_load_protected(header);
  printf("Loading the initramfs...\n");
  load_file(header->ramdisk_image, INITRD_LBA, INITRD_SIZE);

  printf("Hit a key to boot linux\n");
  read_key();
  printf("Now loading linux...\n");
  for (i = 0; i < 50000000; i++)	// so sexy
    ;
  clean_screen();
  // return to real-mode and jump on linux.
  linux_boot(); // should never return
  hang("Error durring loading linux kernel!");
}
